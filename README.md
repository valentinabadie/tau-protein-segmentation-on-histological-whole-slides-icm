This project is about detecting and segmenting Tau proteins appearing in the brain with Alzheimer's disease.

This task is to be performed on histological whole slides, which are about one gigapixel.

For this purpose, the architecture of the code proposed is built as follows :

    1 - A module to handle easily the histological whole slides.
    
    2 - A module to generate properly datasets to be used for machine learning purposes.
    
    3 - A module to actually perform the deep learning tasks.
    
    
## 1 - Module to easily handle the histological whole slides

The slides are huge images of with different levels of magnification - here the magnification goes from x0.3125 to x40. Hence, the code is built to make it easy to jump into different levels as such.

For the highest resolution level, the resulting images are too huge to be processed as single piece, and hence a way to access to a given region is provided.

Important thing to notice : since there are different magnification levels, all x and y positions on images are given in meter - i.e. corresponding to the real measurements of the studied slice.

The UML diagram is as follows :




An example of use is as follows :
`D = DataSetSlides(path_slides = 'your/path/to/folder/containing/slides', path_labels = 'your/path/to/folder/containing/slides', path_infos = 'your/path/to/folder/containing/infos')`

`region = D[0]['x10'](-3000,0,1000,3000)`

`print(region)`

This code chooses the rectangular region of which the left inferior corner is at x = -3000 and y = 0, left inferior corner is at x = 1000 and y = 3000, and which belongs to the first slide of the dataset provided, at the magnification level x10 and 