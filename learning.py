import os
import numpy as np

from torch.utils.data import DataLoader
import torch

from library.DeepLearningTasks import train, test
from library.Models import UNet
from library.DataSetLearning import DataSplitter, DataAugmentation
from library.utils import SaveFiles as sf
from library.Losses import FocalLoss


#setting the device to be used
torch.cuda.set_device(1)
torch.cuda.empty_cache()

#setting parameters of the neural network
in_channels = 4
out_channels = 3
depth = 6
dropout_conv = 0.
dropout_upconv = 0.

#setting parameters of the training
epochs = 20
batch_size = 8
criterion = FocalLoss(gamma = 2)

#setting parameters of the dataset
level = 3 #magnification = 0=x40, 1=x20, 2=x10, 3=x5,...
size = 256
stride = 256

#loading the dataset
DL = sf.load('/home/valentin.abadie/Project/Data/DataSets/DataSet_Slide_1_Magni_{}_Size_{}_Stride_{}/Dataset.p'.format(level, size, stride))
DL.loaded()

#splitting dataset
DD = DataSplitter(DL)
train_data = DD.datasets['train']
test_data = DD.datasets['test']

#performing data augmentation
train_data_aug = DataAugmentation(train_data, rotation90 = True)

inputs_train = DataLoader(train_data_aug,batch_size=batch_size,shuffle=True)
inputs_test = DataLoader(test_data,batch_size=1,shuffle=False)

#displaying the parameters of the dataset
print('Model : Magnification = {}, Size = {}, Stride = {}'.format(level, size, stride))

#initialising the neural network
model = UNet(in_size=in_channels,out_size=out_channels,depth=depth, dropout_conv = dropout_conv, dropout_upconv = dropout_upconv) 

#performing train
_,_,list_loss_test = train(model, inputs_train, inputs_test, epochs = epochs, path_save = '/home/valentin.abadie/Project/Data/DataSets/DataSet_Slide_1_Magni_{}_Size_{}_Stride_{}'.format(level, size, stride), criterion = criterion)

#finding the best epoch performed regarding at validation loss
best_epoch = np.argmin(list_loss_test)

#removing neural networks that are not optimal towards loss on validation (for memory needs)
for epoch in range(epochs):
    if epoch != best_epoch:
        os.remove('/home/valentin.abadie/Project/Data/DataSets/DataSet_Slide_1_Magni_{}_Size_{}_Stride_{}'.format(level, size, stride) + '/models/' + '/unet_epoch={}_batch={}_loss_{}_{}{}.pt'.format(epoch, batch_size, criterion, dropout_conv, dropout_upconv))
