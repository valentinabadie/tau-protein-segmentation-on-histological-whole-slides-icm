from library.DataSetLearning import DataSetLearning

#set parameters of the dataset
slides = 1
levels = 0
size = 256
stride = 256

#makes the dataset and saves it
DL = DataSetLearning(path_slides=path_slides,path_labels=path_labels,path_infos=path_infos,slides = slides, levels=levels,size = size,stride = stride)
DL.set_path('DataSet_Slide_{}_Magni_{}_Size_{}_Stride_{}'.format(slides, levels, size, stride))
DL.save()